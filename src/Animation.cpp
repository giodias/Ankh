#include "../include/Animation.hpp"

#include "../include/Character.hpp"
#include "../include/logger.hpp"

std::vector<unique_ptr<CarryAnimationState>> CASv;

bool CarryAnimationState::activate(){
    if(!(steps % int(CONFIG::fps*ANIMATION_SPEED))){
        source->entitystatephase++;
    }
    steps--;

    if(!steps){
        if(source->entitystateaction != EntityStateAction::hurt){
            source->phaseUpdate(true);
        }
        return true;
    } else {
        return false;
    }
}

bool CarryMovementState::activate(){
    SFX_PLAYER::play(SFXs::STEP_SFX);
    if (direction == _up) {
        source->updatePosition(0, -1);
    } else if (direction == _down) {
        source->updatePosition(0, 1);
    } else if (direction == _left) {
        source->updatePosition(-1, 0);
    } else if (direction == _right) {
        source->updatePosition(1, 0);
    }
    source->move(direction);
    steps--;

    if(!steps){
        if(source->getPosition().x % source->getTileSize() || source->getPosition().y % source->getTileSize()){
            Logger::getI().criticalError("invalid position at end of movement");
        }
        return true;
    } else {
        return false;
    }

}
