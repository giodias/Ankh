/**============================================================================
 *  Project Name	<Ankh>
 *  Version			<1.0>
 *  Authors		    <Natan Junges>
 *  File Name		<Character.cpp>
/=============================================================================*/
#include <vector>
#include <random>
#include <algorithm>
#include <memory>

#include "../include/Character.hpp"
#include "../include/Ability.hpp"
#include "../include/Entity.hpp"
#include "../include/util.hpp"
#include "../include/logger.hpp"
#include "../include/config.hpp"
#include "../include/AI.hpp"
/**==========================================================================**/

template class AICharacter<RandomAI>;

Character::Character(string _image, int _xPos, int _zPos,
          int _hp, int _mp, int _ap,
          int _maxHp, int _maxMp, int _maxAp):
  Entity(_image.c_str(), 64, 64, _xPos, _zPos),
  hp(_hp, _maxHp), mp(_mp, _maxMp), ap(_ap, _maxAp){



    buffs.push_back(stats_buff("end turn default", 0, +1, 5, false));

    abilities = std::vector<unique_ptr<Ability>>();

    abilities.push_back(move_ability("move up", 0, 0, -1, ALLEGRO_KEY_UP, _up));
    abilities.push_back(move_ability("move left", 0, 0, -1, ALLEGRO_KEY_LEFT, _left));
    abilities.push_back(move_ability("move down", 0, 0, -1, ALLEGRO_KEY_DOWN, _down));
    abilities.push_back(move_ability("move right", 0, 0, -1, ALLEGRO_KEY_RIGHT, _right));
}

PlayerCharacter::PlayerCharacter(string _image, int _xPos, int _zPos, int _hp, int _mp, int _ap, int _maxHp, int _maxMp, int _maxAp):
  Character(_image, _xPos, _zPos, _hp, _mp, _ap, _maxHp, _maxMp, _maxAp)
{
    std::uniform_int_distribution<> dis1(0,4);

    switch (dis1(gen)) {
    case 0:
        buffs.push_back(stats_buff("Life Regen (+2 HP/turn)", +2, 0, 0, true));
        break;
    case 1:
        buffs.push_back(stats_buff("Mana Regen (+2 MP/turn)", 0, +2, 0, true));
        break;
    case 2:
        buffs.push_back(stats_buff("Quick (+1 AP/turn)", 0, 0, +1, true));
        ap.bDiff(+1);
        break;
    case 3:
        buffs.push_back(stats_buff("Strong (+5 HP, +1/turn)", 1, 0, 0, true));
        hp.bDiff(+5);
        break;
    case 4:
        buffs.push_back(stats_buff("Mage (+5 MP, +1/turn)", 0, 1, 0, true));
        mp.bDiff(+5);
        break;
    }

    std::uniform_int_distribution<> dis2(0,2);
    switch (dis2(gen)) {
    case 0:
        buffs.push_back(stats_buff("Dagger (3 DMG/-3AP)", 0, 0, 0, true, false, al_map_rgba(255,255,255,255), al_load_bitmap("resources/sprites/dagger.png")));
        abilities.push_back(target_stats_ability("(A)ttack", 0, 0, -3, ALLEGRO_KEY_A, -3, 0, 0, TILE_SIZE, true, SFXs::PUNCH_SFX, EntityStateAction::slash));
        break;
    case 1:
        buffs.push_back(stats_buff("Spear (6 DMG/-5AP)", 0, 0, 0, true, false, al_map_rgba(255,255,255,255), al_load_bitmap("resources/sprites/spear.png")));
        abilities.push_back(target_stats_ability("(A)ttack", 0, 0, -5, ALLEGRO_KEY_A, -6, 0, 0, TILE_SIZE*2, true, SFXs::PUNCH_SFX, EntityStateAction::thrust));
        break;
    case 2:
        buffs.push_back(stats_buff("Bow and arrow (4 DMG, Range 5 /-5AP)", 0, 0, 0, true, false, al_map_rgba(255,255,255,255), al_load_bitmap("resources/sprites/bow.png")));
        buffs.push_back(stats_buff("arrow", 0, 0, 0, false, false, al_map_rgba(255,255,255,255), al_load_bitmap("resources/sprites/arrow.png")));
        abilities.push_back(target_stats_ability("(A)ttack", 0, 0, -5, ALLEGRO_KEY_A, -5, 0, 0, 5*TILE_SIZE, true, SFXs::PUNCH_SFX, EntityStateAction::shoot));
        break;
    }

    abilities.push_back(teleport_ability("(T)eleport", 0, -5, -5, ALLEGRO_KEY_T, SFXs::TELEPORT_SFX));
    abilities.push_back(friendly_stats_ability("(H)eal", +2, -1, -2, ALLEGRO_KEY_H, EntityStateAction::casting, true, SFXs::TELEPORT_SFX));
    abilities.push_back(friendly_stats_ability("(M)editate", 0, +1, -2, ALLEGRO_KEY_M, EntityStateAction::casting, true, SFXs::TELEPORT_SFX));

    abilities.push_back(target_stats_ability("(B)urn", 0, -6, -5, ALLEGRO_KEY_B, -3, 0, 0, INT_MAX,
                                             [](Character& source) -> unique_ptr<Buff> {
                                                 auto bte = std::vector<std::unique_ptr<Effect>>();
                                                 bte.push_back(unique_ptr<Effect>(new HostileStatsEffect(-2,0,0)));
                                                 return std::unique_ptr<Buff>(new TemporaryBuff(&source,
                                                                                                "Burning",
                                                                                                true,
                                                                                                3,
                                                                                                std::move(bte),
                                                                                                true,
                                                                                                al_map_rgba(255, 150, 150, 255)));},
                                             true, SFXs::FIREBALL_SFX, EntityStateAction::casting)
                        );

   abilities.push_back(target_stats_ability("(R)ay of Light", 0, -6, -5, ALLEGRO_KEY_R, -6, 0, 0, INT_MAX,
                                             [](Character& source) -> unique_ptr<Buff> {return
                                                     std::unique_ptr<Buff>(new TemporaryBuff(&source,
                                                                                       "Light",
                                                                                       true,
                                                                                       1,
                                                                                       std::vector<std::unique_ptr<Effect>>(),
                                                                                       true,
                                                                                       al_map_rgba(255, 255, 255, 150)));},
                                             true, SFXs::FIREBALL_SFX, EntityStateAction::casting)
                        );

    abilities.push_back(end_turn_ability("(E)nd turn", ALLEGRO_KEY_E));
}

template <> AICharacter<RandomAI>::AICharacter(string _image, int _xPos, int _zPos, int _hp, int _mp, int _ap, int _maxHp, int _maxMp, int _maxAp):
  Character(_image, _xPos, _zPos, _hp, _mp, _ap, _maxHp, _maxMp, _maxAp),
  ai(RandomAI(this))
{
    std::uniform_int_distribution<> dis1(0,4);

    switch (dis1(gen)) {
    case 0:
        buffs.push_back(stats_buff("Life Regen (+2 HP/turn)", +1, 0, 0, true));
        break;
    case 1:
        buffs.push_back(stats_buff("Mana Regen (+2 MP/turn)", 0, +1, 0, true));
        break;
    case 2:
        buffs.push_back(stats_buff("Quick (+1 AP/turn)", 0, 0, +1, true));
        ap.bDiff(+1);
        break;
    case 3:
        buffs.push_back(stats_buff("Strong (+5 HP, +1/turn)", 0, 0, 0, true));
        hp.bDiff(+5);
        break;
    case 4:
        buffs.push_back(stats_buff("Mage (+5 MP, +1/turn)", 0, 0, 0, true));
        mp.bDiff(+5);
        break;
    }

    abilities.push_back(target_stats_ability("(P)unch", 0, 0, -3, ALLEGRO_KEY_P, -3, 0, 0, TILE_SIZE, true, SFXs::PUNCH_SFX, EntityStateAction::slash));

    abilities.push_back(end_turn_ability("(E)nd turn", ALLEGRO_KEY_E));
}

template <> AICharacter<RandomAI>* AICharacter<RandomAI>::random_monster(World& world, string image, Coord coord, int hp, int mp, int ap, int maxHp, int maxMp, int maxAp){
    auto aic = new AICharacter<RandomAI>(image, coord.x, coord.y, hp, mp, ap, maxHp, maxMp, maxAp);
    world.insertEntity(aic);
    return aic;
}

template <> AICharacter<RandomAI>* AICharacter<RandomAI>::random_monster(World& world, string image, int hp, int mp, int ap, int maxHp, int maxMp, int maxAp){
    return random_monster(world, image, world.random_position(), hp, mp, ap, maxHp, maxMp, maxAp);
}




template <> AICharacter<RandomAI>* AICharacter<RandomAI>::shadow_monster(World& world, Coord coord, int hp, int mp, int ap, int maxHp, int maxMp, int maxAp){
    auto aic = random_monster(world, "shadow", coord, hp, mp, ap, maxHp, maxMp, maxAp);
    aic->buffs.push_back(aic->stats_buff("shadow", 0, 0, 0, false, true, al_map_rgba(255, 255, 255, 150)));

    return aic;
}
template <> AICharacter<RandomAI>* AICharacter<RandomAI>::random_monster(World& world, int hp, int mp, int ap, int maxHp, int maxMp, int maxAp){
    std::uniform_int_distribution<> dis(0,3);

    switch (dis(gen)) {
    case 0:
        return shadow_monster(world, world.random_position(), hp, mp, ap, maxHp, maxMp, maxAp);
    case 1:
        return random_monster(world, "sandmonster", world.random_position(), hp, mp, ap, maxHp, maxMp, maxAp);
    case 2:
        return random_monster(world, "stripes_mummy", world.random_position(), hp, mp, ap, maxHp, maxMp, maxAp);
    case 3:
        return random_monster(world, "sandmonster2", world.random_position(), hp, mp, ap, maxHp, maxMp, maxAp);
    }
    Logger::getI().criticalError("rng error");
    return nullptr;
}
template <> AICharacter<RandomAI>* AICharacter<RandomAI>::shadow_monster(World& world, int hp, int mp, int ap, int maxHp, int maxMp, int maxAp){
    return shadow_monster(world, world.random_position(), hp, mp, ap, maxHp, maxMp, maxAp);
}

template <> AICharacter<RandomAI>* AICharacter<RandomAI>::level_monster(World& world, int level){
    auto rm = random_monster(world, 6, 5, 5, 6, 5, 5);
    while(level > 0){
        std::uniform_int_distribution<> dis1(0,2);
        switch (dis1(gen)) {
        case 0:
            rm->hp.bDiff(1);
            rm->buffs.push_back(rm->stats_buff("hp", +1, 0, 0, true));
            break;
        case 1:
            rm->mp.bDiff(1);
            rm->buffs.push_back(rm->stats_buff("mp", 0, 1, 0, true));
            break;
        case 2:
            rm->ap.bDiff(1);
            rm->buffs.push_back(rm->stats_buff("ap", 0, 0, 1, true));
            break;
        }
        level--;
    }
    return rm;
}

PlayerCharacter* PlayerCharacter::def_pc(World& world, string image, Coord coord, int hp, int mp, int ap, int maxHp, int maxMp, int maxAp){
    auto pc = new PlayerCharacter(image, coord.x, coord.y, hp, mp, ap, maxHp, maxMp, maxAp);
    world.insertEntity(pc);
    return pc;
}

void Character::drawSprite(coord origin, ALLEGRO_COLOR ){
    std::vector<ALLEGRO_COLOR> colors;

    for(auto b = buffs.begin(); b != buffs.end(); b++){
        if ((*b)->use_color){
            colors.push_back((*b)->color);
        }
    }

    ALLEGRO_COLOR merged_color;

    if(colors.empty()){
        Entity::drawSprite(origin);
    } else {
        merged_color = merge_colors(colors);
        Entity::drawSprite(origin, merged_color);
    }

    for(auto b = buffs.begin(); b != buffs.end(); b++){
        if ((*b)->bitmap){
            if(colors.empty()){
                drawOtherSprite((*b)->bitmap, origin);
            } else {
                drawOtherSprite((*b)->bitmap, origin, merged_color);
            }
        }
    }
}

unique_ptr<Buff> Character::stats_buff(char const * const name, int hpDiff, int mpDiff, int apDiff, bool show, bool use_color, ALLEGRO_COLOR color, ALLEGRO_BITMAP* bitmap){
    auto v1 = std::vector<unique_ptr<Effect>>();
    v1.push_back(std::unique_ptr<Effect>(new FriendlyStatsEffect(hpDiff, mpDiff, apDiff)));

    return std::unique_ptr<Buff>(new Buff(this, name, show, std::move(v1), use_color, color, bitmap));
}

unique_ptr<Buff> Character::temporary_stats_buff(char const * const name, int hpDiff, int mpDiff, int apDiff, int end_turn, bool show, bool use_color, ALLEGRO_COLOR color){
    auto v1 = std::vector<unique_ptr<Effect>>();
    v1.push_back(std::unique_ptr<Effect>(new FriendlyStatsEffect(hpDiff, mpDiff, apDiff)));

    return std::unique_ptr<Buff>(new TemporaryBuff(this, name, show, end_turn, std::move(v1), use_color, color));
}

bool Character::has_buff(const std::string& buff_name){
    return (std::any_of(buffs.begin(), buffs.end(), [buff_name](std::unique_ptr<Buff> &b){return !(b->name.compare(buff_name));}));
}

unique_ptr<Ability> Character::end_turn_ability(char const * const name, int keycode, bool show){
  auto v1 = std::vector<unique_ptr<Effect>>();
  v1.push_back(std::unique_ptr<Effect>(new EndTurn()));

  return std::unique_ptr<Ability>(new Ability(this, name, show, keycode, std::move(v1)));
}

unique_ptr<Ability> Character::friendly_stats_ability(char const * const name, int hpDiff, int mpDiff, int apDiff, int keycode, bool show, SFXs sfxs){
  auto v1 = std::vector<unique_ptr<Effect>>();
  v1.push_back(std::unique_ptr<Effect>(new FriendlyStatsEffect(hpDiff, mpDiff, apDiff)));

  return std::unique_ptr<Ability>(new Ability(this, name, show, keycode, std::move(v1), sfxs));
}

unique_ptr<Ability> Character::friendly_stats_ability(char const * const name, int hpDiff, int mpDiff, int apDiff, int keycode, EntityStateAction esa, bool show, SFXs sfxs){
  auto ab = friendly_stats_ability(name, hpDiff, mpDiff, apDiff, keycode, show, sfxs);

  ab->effects.effects.push_back(std::unique_ptr<Effect>(new Animate(esa)));

  return ab;
}

unique_ptr<Ability> Character::target_stats_ability(char const * const name, int hpDiff, int mpDiff, int apDiff, int keycode, int thpDiff, int tmpDiff, int tapDiff, int range, bool show, SFXs sfxs, EntityStateAction esa){
    auto vs = std::vector<unique_ptr<Effect>>();
    vs.push_back(std::unique_ptr<Effect>(new FriendlyStatsEffect(hpDiff, mpDiff, apDiff)));
    vs.push_back(std::unique_ptr<Effect>(new Animate(esa)));

    auto vt = std::vector<unique_ptr<EffectST>>();
    vt.push_back(std::unique_ptr<EffectST>(new EffectT(std::unique_ptr<Effect>(new HostileStatsEffect(thpDiff, tmpDiff, tapDiff)))));
    vt.push_back(std::unique_ptr<EffectST>(new EffectT(std::unique_ptr<Effect>(new AnimateIfDead(EntityStateAction::hurt)))));
    vt.push_back(std::unique_ptr<EffectST>(new RotateToTarget()));

    std::vector<TargetEffectGroup> teg;
    teg.push_back(TargetEffectGroup(this, nullptr, std::move(vt), AbilityTargetting{range}));

    return std::unique_ptr<Ability>(new Ability(this, name, show, keycode, std::move(vs), sfxs, std::move(teg)));
}

unique_ptr<Ability> Character::target_stats_ability(char const * const name, int hpDiff, int mpDiff, int apDiff, int keycode, int thpDiff, int tmpDiff, int tapDiff, int range, std::function<std::unique_ptr<Buff>(Character&)> tb, bool show, SFXs sfxs, EntityStateAction esa){
    auto ttsa = target_stats_ability(name, hpDiff, mpDiff, apDiff, keycode, thpDiff, tmpDiff, tapDiff, range, show, sfxs, esa);
    ttsa->targetEffects[0].effects.push_back(std::unique_ptr<EffectST>(new EffectT(std::unique_ptr<Effect>(new BuffEffect(tb)))));
    return ttsa;
}

unique_ptr<Ability> Character::teleport_ability(char const * const name, int hpDiff, int mpDiff, int apDiff, int keycode, SFXs sfxs){
    auto ability = friendly_stats_ability(name, hpDiff, mpDiff, apDiff, keycode, true, sfxs);

    ability->effects.effects.push_back(std::unique_ptr<Effect>(new RandomTeleport()));
    ability->effects.effects.push_back(std::unique_ptr<Effect>(new Animate(EntityStateAction::casting)));

    return ability;
}

unique_ptr<Ability> Character::move_ability(char const * const name, int hpDiff, int mpDiff, int apDiff, int keycode, Direction direction){
    auto ability = friendly_stats_ability(name, hpDiff, mpDiff, apDiff, keycode, false);

    ability->effects.effects.push_back(std::unique_ptr<Effect>(new Move(direction)));

    return ability;
}

unique_ptr<Ability>* Character::activate(int keycode){

    auto gl = [keycode](unique_ptr<Ability>& a){return a->keycode == keycode;};
    auto matched_ability = std::find_if(std::begin(abilities), std::end(abilities), gl);

    if(matched_ability != std::end(abilities)){
        return &*matched_ability;
    } else {
        return nullptr;
    }
}

void Character::end_turn(int turn, World& world) {

    std::vector<int> remove_buffs = std::vector<int>();

    for(auto b = buffs.begin(); b != buffs.end() ;b++) {
        bool keep = (*b)->end_turn(turn, world);
        if(!keep){
            remove_buffs.push_back(b-buffs.begin());
        }
    }

    while(!remove_buffs.empty()){
        int a = remove_buffs.back();
        remove_buffs.pop_back();
        buffs.erase(buffs.begin()+a);
    }
}

void Character::activateAnimation(EntityStateAction esa){
    if(esa == EntityStateAction::hurt){
        entitystatedirection = EntityStateDirection::up;
    }
    entitystateaction = esa;
    entitystatephase = 0;

    CASv.push_back(unique_ptr<CarryAnimationState>(new CarryAnimationState(this, (NumberOfPhases(esa)-1)*int(CONFIG::fps*ANIMATION_SPEED))));
}

void Character::storegame(sqlite3* db, std::string& savename){
  //todo
  //buff.storegame();
}
void Character::loadgame(sqlite3* db, std::string& savename){
  //todo
  //buff.loadgame();
}
