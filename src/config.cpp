/**==================================================================
//  Project: <Ankh>
//  Version: <1.0>
//  Author:  <Matheus Dias>
//  File:    <config.cpp>
//================================================================**/
#include "../include/config.hpp"
#include <stdio.h>
#include <iostream>
/**===============================================================**/
ALLEGRO_CONFIG* CONFIG::cfg = NULL;
unsigned int CONFIG::fps = DEFAULT_FPS;
unsigned int CONFIG::screen_w = DEFAULT_SCREEN_W;
unsigned int CONFIG::screen_h = DEFAULT_SCREEN_H;
double CONFIG::gain = DEFAULT_GAIN;
double CONFIG::sfx_gain = DEFAULT_GAIN*0.89;

bool CONFIG::openFile(const char* path) {
    CONFIG::cfg = al_load_config_file(path);
    if (!cfg) {
        return false;
    } else {
        return true;
    }
}
void CONFIG::saveFile(const char* path) {
    char buf[50];
    if (!CONFIG::cfg) {
        CONFIG::cfg = al_create_config();
    }
    sprintf(buf,"%u", CONFIG::screen_w);
    al_set_config_value(CONFIG::cfg, "SCREEN", "W", buf);
    sprintf(buf, "%u", CONFIG::screen_h);
    al_set_config_value(CONFIG::cfg, "SCREEN", "H", buf);
    sprintf(buf, "%u", CONFIG::fps);
    al_set_config_value(CONFIG::cfg, "SCREEN", "FPS", buf);
    sprintf(buf,"%f",CONFIG::gain);
    al_set_config_value(CONFIG::cfg,"AUDIO","GAIN",buf);
    sprintf(buf,"%f",CONFIG::sfx_gain);
    al_set_config_value(CONFIG::cfg,"AUDIO","SFX_GAIN",buf);
    al_save_config_file(path, CONFIG::cfg);
}
void CONFIG::destrConf() {
    al_destroy_config(CONFIG::cfg);
    CONFIG::cfg = NULL;
}
void CONFIG::loadConf() {
    const char* buf;
    buf = al_get_config_value(CONFIG::cfg, "SCREEN", "FPS");
    if(buf)
        sscanf(buf,"%u", &(CONFIG::fps));
    buf = al_get_config_value(CONFIG::cfg, "SCREEN", "H");
    if(buf)
        sscanf(buf,"%u", &(CONFIG::screen_h));
    buf = al_get_config_value(CONFIG::cfg, "SCREEN", "W");
    if(buf)
        sscanf(buf,"%u", &(CONFIG::screen_w));
    buf = al_get_config_value(CONFIG::cfg,"AUDIO","GAIN");
    if(buf)
        sscanf(buf,"%lf",&(CONFIG::gain));
    buf = al_get_config_value(CONFIG::cfg,"AUDIO","SFX_GAIN");
    if(buf)
        sscanf(buf,"%lf",&(CONFIG::sfx_gain));
}
void CONFIG::creatConf() {
    CONFIG::cfg = al_create_config();
}
/**===============================================================**/
