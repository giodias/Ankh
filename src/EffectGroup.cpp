#include "../include/EffectGroup.hpp"
#include "../include/Effect.hpp"


EffectStatus SourceEffectGroup::active(const World& world){
    EffectStatus acces = EffectStatus::pointless;
    for(auto tg = effects.begin(); tg != effects.end(); tg++){
        EffectStatus es = (*tg)->valid_target(*source, world);

        if(es == EffectStatus::impossible){
            return EffectStatus::impossible;
        } else if (es == EffectStatus::useful){
            acces = EffectStatus::useful;
        }
    }
    return acces;
}

bool TargetEffectGroup::active(
    PlayerCharacter& per,
    // const std::vector<AICharacter<RandomAI>*>& friendlies,
    const std::vector<AICharacter<RandomAI>*>& hostiles,
    const World& world
    ){

    std::vector<Character*> targets;

    calculateTargets(targets, per, hostiles, world);

    if(targets.empty()){
        return false;
    } else {
        return true;
    }
}

void SourceEffectGroup::activate(World& world){
    for(auto e = effects.begin(); e != effects.end(); e++){
        (*e)->activate(*source, world);
    }
}

void TargetEffectGroup::activate(World& world){
  for(auto e = effects.begin(); e != effects.end(); e++){
    (*e)->activate(*source, *target, world);
  }
}


bool TargetEffectGroup::validTarget(const Character& tc, const World& world) const {
    if(source->distance(tc) > target_types.range){
        return false;
    }

    bool acces = false;
    for(auto tg = effects.begin(); tg != effects.end(); tg++){
        EffectStatus es = (*tg)->valid_target(*source, tc, world);

        if(es == EffectStatus::impossible){
            return false;
        } else if (es == EffectStatus::useful){
            acces = true;
        }
    }
    return acces;
}


void TargetEffectGroup::calculateTargets(
    std::vector<Character*>& targets,
    PlayerCharacter& per,
    // const std::vector<AICharacter<RandomAI>*>& friendlies,
    const std::vector<AICharacter<RandomAI>*>& hostiles,
    const World& world
    ){

    targets.clear();

    if(source == &per){
        // Player is acting
        targets.resize(hostiles.size());
        targets.resize(std::distance(targets.begin(), std::copy_if(hostiles.begin(), hostiles.end(), targets.begin(), [this, &world](Character const*const c){return validTarget(*c, world);})));
    } else {
        // AI is acting
        if(source->distance(per) <= target_types.range){
            targets.push_back(&per);
        }
    }
}

void TargetEffectGroup::reset(){
    target = nullptr;
}


