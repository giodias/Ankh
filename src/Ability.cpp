#include <random>

#include "../include/util.hpp"
#include "../include/Ability.hpp"
#include "../include/Entity.hpp"
#include "../include/logger.hpp"

void Ability::activate(World& world, PlayerCharacter& per, std::vector<AICharacter<RandomAI>*> hostiles){
    if(!active(per, hostiles, world)){
        cout << "Activated when inactive" <<endl;
        return;
    }

    SFX_PLAYER::play(sfxs);

    effects.activate(world);

    selected = false;

    for(auto te = targetEffects.begin(); te != targetEffects.end(); te++){
        te->activate(world);
        te->target = nullptr; // reset target
    }
}

void Ability::drawEA(ALLEGRO_FONT* ability_font, int posX, int posY, PlayerCharacter& per, std::vector<AICharacter<RandomAI>*> hostiles, const World& world) {
    auto color = selected ? al_map_rgba(0,0,255,0) : (active(per, hostiles, world) ? al_map_rgba(255,0,0,0) : al_map_rgba(127,127,127,0));
    draw_thing_text(ability_font, posX, posY, name, color);
}

// True if ability can be activated and do something useful
bool Ability::active(
    PlayerCharacter& per,
    // const std::vector<AICharacter<RandomAI>*>& friendlies,
    const std::vector<AICharacter<RandomAI>*>& hostiles,
    const World& world
    ){

    EffectStatus sees = effects.active(world);

    if(sees == EffectStatus::impossible){
        return false;
    }

    for(auto teg = targetEffects.begin(); teg != targetEffects.end(); teg++){
        bool tees = teg->active(per, hostiles, world);
        if(tees){
            sees = EffectStatus::useful;
        } else {
            return false;
        }
    }

    return EffectStatus::useful == sees;
}


bool Ability::validTarget(const Character& tc, const World& world) const {
    for(auto tg = targetEffects.begin(); tg != targetEffects.end(); tg++){
        if(!tg->validTarget(tc, world)){
            return false;
        }
    }
    return true;
}

void Ability::insertTarget(Character * const tc, const World& world){
    for(auto tg = targetEffects.begin(); tg != targetEffects.end(); tg++){
        if(!tg->target){
            if(!validTarget(*tc, world)){
                Logger::getI().criticalError("invalid target");
                return;
            }
            tg->target = tc;
            return;
        }
    }
    Logger::getI().criticalError("nothing to target");
}

// True if targeting is done
bool Ability::calculateTargets (
    std::vector<Character*>& targets,
    PlayerCharacter& per,
    // const std::vector<AICharacter<RandomAI>*>& friendlies,
    const std::vector<AICharacter<RandomAI>*>& hostiles,
    const World& world
    ){

    targets.clear();

    auto missing_target = std::find_if(targetEffects.begin(), targetEffects.end(), [](TargetEffectGroup& teg){return !teg.target;});
    if (missing_target == targetEffects.end()){ // No missing target, fire
        return true;
    } else {
        missing_target->calculateTargets(targets, per, hostiles, world);
        return false;
    }
}

void Ability::reset(){
    for(auto te = targetEffects.begin(); te != targetEffects.end(); te++){
        te->reset();
    }
}

