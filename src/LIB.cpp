/**============================================================================
 *  Project Name    <Ankh>
 *  Version         <1.0>
 *  Authors         <Matheus Dias>
 *  File Name       <LIB.cpp>
/=============================================================================*/
#include "../include/LIB.hpp"
/**=========================================================================**/
using namespace std;
/**=========================================================================**/
///LIB
template<class _TP>
_TP LIB<_TP>::operator[](unsigned int i){
    return(this->data[i]);
}

template<class _TP>
void LIB<_TP>::add(_TP element){
    this->data.push_back(element);
}

template<class _TP>
unsigned int LIB<_TP>::size(void){
    return(this->data.size());
}
/**=========================================================================**/
LIB_BITMAP::~LIB_BITMAP(){
    this->free();
}

void LIB_BITMAP::free(void){
    vector<ALLEGRO_BITMAP*>::iterator itr;
    unsigned int i;
    for(itr=(this->data.begin()),i=0;itr!=(this->data.end());itr++,i++){
        al_destroy_bitmap(this->data[i]);
    }
    this->data.clear();
}
/**=========================================================================**/
LIB_GUI::~LIB_GUI(){
    this->free();
}

void LIB_GUI::free(){
    vector<GUI*>::iterator itr;
    unsigned int i;
    for(itr=(this->data.begin()),i=0;itr!=(this->data.end());itr++,i++){
        delete (this->data[i]);
    }
    this->data.clear();
}
/**=========================================================================**/
LIB_SFX::~LIB_SFX(){
    this->free();
}

void LIB_SFX::free(){
    vector<ALLEGRO_SAMPLE*>::iterator itr;
    unsigned int i;
    for(itr=(this->data.begin()),i=0;itr!=(this->data.end());itr++,i++){
        al_destroy_sample(this->data[i]);
    }
    this->data.clear();
}
/**=========================================================================**/
