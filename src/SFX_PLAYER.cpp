/**============================================================================
 *  Project Name    <Ankh>
 *  Version         <1.0>
 *  Authors         <Matheus Dias>
 *  File Name       <SFX_PLAYER.cpp>
/=============================================================================*/
#include "../include/SFX_PLAYER.hpp"
#include "../include/config.hpp"
#include "../include/allegro.h"
/**=========================================================================**/
LIB_SFX SFX_PLAYER::sfx;

void SFX_PLAYER::play(SFXs s){
    if(SFX_PLAYER::sfx[(unsigned int)s])
        al_play_sample(SFX_PLAYER::sfx[(unsigned int)s],CONFIG::sfx_gain,0,1,
                       ALLEGRO_PLAYMODE_ONCE,NULL);
    else
        al_stop_samples();
}

void SFX_PLAYER::stop(void){
    al_stop_samples();
}

void SFX_PLAYER::free(void){
    SFX_PLAYER::sfx.free();
}

void SFX_PLAYER::load(void){
    SFX_PLAYER::sfx.add(NULL);
    SFX_PLAYER::sfx.add(al_load_sample("resources/audio/sfx/punch.wav"));
    SFX_PLAYER::sfx.add(al_load_sample("resources/audio/sfx/step.ogg"));
    SFX_PLAYER::sfx.add(al_load_sample("resources/audio/sfx/teleport.wav"));
    SFX_PLAYER::sfx.add(al_load_sample("resources/audio/sfx/fireball.wav"));
}
/**=========================================================================**/
