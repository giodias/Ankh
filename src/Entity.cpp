
#define SPRITES_PATH "resources/sprites/"

#include "../include/Ankh.hpp"
#include "../include/Entity.hpp"
#include "../include/logger.hpp"
#include "../include/config.hpp"
#include "../include/util.hpp"


using namespace std;

int frameCounter = 0;
map<int, Entity*> *Entity::entities;
int Entity::worldWidth;
int Entity::worldHeight;
int *Entity::nextId;
int Entity::tileSize;
int Entity::screenWidth;
int Entity::screenHeight;

int NumberOfPhases(EntityStateAction esa){
    switch(esa){
    case EntityStateAction::casting:
        return 7;
    case EntityStateAction::thrust:
        return 8;
    case EntityStateAction::walk:
        return 9;
    case EntityStateAction::slash:
        return 6;
    case EntityStateAction::shoot:
        return 13;
    case EntityStateAction::hurt:
      return 6;
    }
    Logger::getI().criticalError("NumberOfPhases: invalid action");
    return 0;
}

EntityStateDirection dir2esa(Direction d){
    switch(d){
    case _left:
        return EntityStateDirection::left;
    case _right:
        return EntityStateDirection::right;
    case _up:
        return EntityStateDirection::up;
    case _down:
    case _stop:
        return EntityStateDirection::down;
    }
    Logger::getI().criticalError("invalid direction");
    return EntityStateDirection::down;
}

Entity::Entity(char const * path, int width, int height, int x, int y,
               EntityStateDirection esd,
               EntityStateAction esa,
               EntityStatePhase esp):
    entitystatedirection(esd),
    entitystateaction(esa),
    entitystatephase(esp)
{
    string mes;
    mes = SPRITES_PATH + string(path) + ".png";
    sprite = al_load_bitmap(mes.data());
    if (!(this->sprite)) {
        mes = "Could not load the Entity " + string(path) + " sprite!";
        Logger::getI().criticalError(mes.data());
    }
    this->width = width;
    this->height = height;
    position.x = initialPosition.x = x;
    position.y = initialPosition.y = y;
    blocked = false;
    name = _Entity;
}
Entity::~Entity() {
    al_destroy_bitmap(sprite);
    entities->erase(id);
    cout<<"Deleting Entity id: "<<id<<endl;
}
void Entity::insertWorld() {
    id = (*nextId)++;
    (*entities)[id] = this;
}
void Entity::setId(int id) {
    this->id = id;
}
coord Entity::getInitialPos() {
    return initialPosition;
}
int Entity::getX() const {
    return position.x;
}
int Entity::getRealX(int x, int y) {
    return (-10) + x * getScale(y) + (getScale(screenHeight) - getScale(y)) * screenWidth / 2.0;
}
int Entity::getY() const {
    return position.y;
}
int Entity::getRealY(int y) {
    return 10 + screenHeight - (worldHeight * tileSize - y) * 3 / 8.0;
}
coord Entity::getPosition() {
    return position;
}
int Entity::getWidth() {
    return width;
}
int Entity::getHeight() {
    return height;
}
float Entity::getScale(int y) {
    return 1 + 2 * (getRealY(y) - screenHeight) / (9.0 * screenWidth);
}
void Entity::setPosition(coord p) {
    position.x = p.x;
    position.y = p.y;
}
void Entity::reset() {
    setPosition(initialPosition);
}
void Entity::block() {
    blocked = true;
}
void Entity::unblock() {
    blocked = false;
}
int Entity::distance(const Entity &e) const {
    return std::hypot(
        (distanceX(e)),
        (distanceY(e)));
}
int Entity::distanceX(const Entity &e) const {
    return abs((position.x + width / 2) - (e.position.x + e.width / 2));
}
int Entity::distanceY(const Entity &e) const {
    return abs((position.y + height / 2) - (e.position.y + e.height / 2));
}
bool Entity::isAlive() const{
    return true;
}
bool Entity::canMove(int dx, int dy) const{
  map<int, Entity*>::iterator it;
  for (it = entities->begin(); it != entities->end(); it++) {
    if (isAlive() && it->second != this && it->second->isAlive() && (it->second->getClass() != _Character || getClass() != _Character)
        && collide(*(it->second), coord(dx, dy))) {
      return false;
    }
  }
  if (position.x + dx >= 0
      && (position.x + dx ) / tileSize < worldWidth
      && position.y + dy >= 0
      && (position.y + dy) / tileSize < worldHeight) {
    return true;
  }
  return false;
}
bool Entity::updatePosition(int dx, int dy) {
    if(canMove(dx, dy)){
            position.x += dx;
            position.y += dy;
            return true;
    }
    return false;
}
void Entity::drawSprite(coord origin, ALLEGRO_COLOR color) {
    drawOtherSprite(sprite, origin, color);
}
void Entity::drawOtherSprite(ALLEGRO_BITMAP* bitmap, coord origin, ALLEGRO_COLOR color) {
    coord spos(position.x - origin.x, position.y - origin.y);
    float scale = getScale(spos.y);
    al_draw_tinted_scaled_bitmap(bitmap, color,
                                 entitystatephase * width,
                                 (int(entitystateaction)*4 + int(entitystatedirection))*height,
                                 width, height,
                                 getRealX(spos.x, spos.y),
                                 getRealY(spos.y) - height * scale,
                                 width * scale, height * scale, 0);
}
void Entity::drawOnTop(char const * const text, ALLEGRO_FONT* font, coord origin, ALLEGRO_COLOR color){
    coord spos(position.x - origin.x, position.y - origin.y);
    float scale = getScale(spos.y);
    draw_thing_text(font, getRealX(spos.x, spos.y), getRealY(spos.y) - height * scale, text, color);
}

WhichClass Entity::getClass() const{
    return name;
}
bool Entity::collide(Entity &ent, coord p) const{
    coord c(position.x + width / 2 + p.x, position.y + height / 2 + p.y);
    coord cent(ent.position.x + ent.width / 2, ent.position.y + ent.height / 2);
    int m = height / width;
    //up
    if (cent.y >= position.y + p.y - ent.height / 2
        && cent.y <= c.y
        && cent.x >= c.x + (c.y - cent.y) / m
        && cent.x <= c.x + (cent.y - c.y) / m) {
        return true;
    //down
    } else if (cent.y >= c.y
               && ent.position.y <= c.y + height / 2
               && cent.x >= c.x + (cent.y - c.y) / m
               && cent.x <= c.x + (c.y - cent.y) / m) {
        return true;
    //left
    } else if (cent.x >= position.x + p.x - ent.width / 2
               && cent.x <= c.x
               && cent.y >= c.y + (c.x - cent.x) * m
               && cent.y <= c.y + (cent.x - c.x) * m) {
        return true;
    //right
    } else if (cent.x >= c.x
               && ent.position.x <= c.x + width / 2
               && cent.y >= c.y + (cent.x - c.x) * m
               && cent.y <= c.y + (c.x - cent.x) * m) {
        return true;
    }
    return false;
}
Direction Entity::backDirection(Entity &ent) {
    if (collide(ent, coord(0, 1))) {
        return _up;
    } else if (collide(ent, coord(1, 0))) {
        return _left;
    } else if (collide(ent, coord(-1, 0))) {
        return _right;
    } else if (collide(ent, coord(0, -1))) {
        return _down;
    }
    Logger::getI().criticalError("Critical Error Entity::backDirection");
    return _stop;
}
Direction Entity::closestDirection(const Entity &ent) {
    if (position == ent.position){
        return _stop;
    }

    int xd = ent.position.x - position.x;
    int yd = ent.position.y - position.y;

    if (abs(xd) >= abs(yd)) {
        if (xd > 0) {
            return _right;
        } else {
            return _left;
        }
    } else {
        if (yd > 0) {
            return _down;
        } else {
            return _up;
        }
    }
}
void Entity::setNewMap(map<int, Entity*> *ent, int w, int h, int *next, int ts) {
    entities = ent;
    worldWidth = w;
    worldHeight = h;
    nextId = next;
    tileSize = ts;
}
void Entity::phaseUpdate(bool end) {
    if (end){
        entitystatephase = 0;
    } else {
        entitystatephase = frameCounter % NumberOfPhases(entitystateaction);
    }
}
void Entity::move(Direction dir) {

    switch(dir){
    case _stop:
        phaseUpdate(true);
        break;
    case _up:
        phaseUpdate(false);
        entitystatedirection = EntityStateDirection::up;
        break;
    case _left:
        phaseUpdate(false);
        entitystatedirection = EntityStateDirection::left;
        break;
    case _down:
        phaseUpdate(false);
        entitystatedirection = EntityStateDirection::down;
        break;
    case _right:
        phaseUpdate(false);
        entitystatedirection = EntityStateDirection::right;
        break;
    }
}
int Entity::getTileSize() {
    return tileSize;
}
