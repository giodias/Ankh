/**============================================================================
 *  Project Name    <Ankh>
 *  Version         <1.0>
 *  Authors         <Ricardo Ardissone>
 *  File Name       <logger.cpp>
/=============================================================================*/
#include "../include/logger.hpp"
#include <stdlib.h>
#define BUFLEN 24
/**==========================================================================**/
using namespace std;
void Logger::open_log_file(const char* path) {
    ///Inicializa Arquivo de Log
    if (Logger::log_file.is_open()) {
        return;
    }
    Logger::log_file.open(path, ofstream::out | ofstream::trunc);
    Logger::log_file << "ANKH LOGFILE" << endl << endl;
}
void Logger::close_log_file() {
    log_file.close();
}
void Logger::printLog(const string message) {
    time_t raw_time;
    tm* time_info;
    char ascii_time[BUFLEN];
    time(&raw_time);
    time_info = localtime(&raw_time);
    strftime(ascii_time, BUFLEN, "<%F - %T>", time_info);
    log_file << ascii_time << ' ' << message << endl;
    cout << ascii_time << ' ' << message << endl;
}
void Logger::operator <<(const string mensage) {
    Logger::printLog(mensage);
}
Logger& Logger::getI() {
    static Logger single;
    return single;
}
void Logger::criticalError(const string message) {
    printLog(message);
    exit(1);
}
Logger::Logger() {
    this->open_log_file(LOG_FILE_PATH);
}
Logger::~Logger() {
    this->close_log_file();
}
