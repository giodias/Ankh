#include "../include/Screen.hpp"
Screen::Screen(World &world, int width = 640, int height = 480) {
    position.x = world.getScreenPos().x;
    position.y = world.getScreenPos().y;
    this->world = &world;
    this->width = width;
    this->height = height;
    Entity::screenWidth = width;
    Entity::screenHeight = height;
    Entity::setNewMap(world.getEntities(),  world.getWidth(), world.getHeight(), world.getNumEntities(), world.getTileSize());
}
Screen::~Screen() {}
void Screen::setWorld(World &world) {
    this->world->setScreenPos(position);
    this->world->resetEntities();
    this->world = &world;
    position.x = this->world->getScreenPos().x;
    position.y = this->world->getScreenPos().y;
    Entity::setNewMap(world.getEntities(), world.getWidth(), world.getHeight(), world.getNumEntities(), world.getTileSize());
}
World *Screen::getWorld() {
    return world;
}
void Screen::clearBuffer() {
    al_flip_display();
    al_clear_to_color(al_map_rgb(0, 0, 0));
}
void Screen::updatePosition(const Entity &character) {
    if (position.x >= 0
        && position.x + width <= world->getRealWidth()
        && character.getX() >= width / 2
        && character.getX() + width / 2 <= world->getRealWidth()) {
        position.x = character.getX() - width / 2;
    }
    if (position.y >= 0
        && position.y + height <= world->getRealHeight()
        && character.getY() >= height / 2
        && character.getY() + height / 2 <= world->getRealHeight()) {
        position.y = character.getY() - height / 2;
    }
}
void Screen::selectVisible() {
    world->selectVisibleEntities(position, width, height);
}
void Screen::draw() {
    world->draw(position, width, height);
    world->drawEntities(position);
}
