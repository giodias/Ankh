#ifndef UTIL_CPP_INCLUDED
#define UTIL_CPP_INCLUDED

#include <random>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include "../include/allegro.h"
#include "../include/Entity.hpp"


template <typename TYPE>
std::string convertToString(TYPE t){
  std::stringstream ss;
  ss<<t;
  return(ss.str());
}

extern std::default_random_engine rd;
extern std::mt19937 gen;

void draw_thing_text(ALLEGRO_FONT* font, int posX, int posY, char const * const thing, ALLEGRO_COLOR color=al_map_rgba(255,0,0,0), int flags=ALLEGRO_ALIGN_LEFT);

void draw_thing_text(ALLEGRO_FONT* font, int posX, int posY, std::string const &thing, ALLEGRO_COLOR color=al_map_rgba(255,0,0,0), int flags=ALLEGRO_ALIGN_LEFT);

void draw_thing_text(ALLEGRO_FONT* font, int posX, int posY, std::stringstream const &thing, ALLEGRO_COLOR color=al_map_rgba(255,0,0,0), int flags=ALLEGRO_ALIGN_LEFT);

ALLEGRO_COLOR merge_colors(const std::vector<ALLEGRO_COLOR>& colors);

#endif //UTIL_CPP_INCLUDED
