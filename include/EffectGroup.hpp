#ifndef EFFECTGROUP_HPP_INCLUDED
#define EFFECTGROUP_HPP_INCLUDED

#include <memory>
#include <vector>

#include "AbilityTargeting.hpp"
#include "World.hpp"

class Ability;
class Character;
class PlayerCharacter;
class RandomAI;
template <typename T> class AICharacter;
class Effect;
class EffectST;

enum class EffectStatus;

class SourceEffectGroup{
    Character * const source;
    std::vector<std::unique_ptr<Effect>> effects;

    public:
    SourceEffectGroup(Character* _source, std::vector<std::unique_ptr<Effect>> _effects):
        source(_source),
        effects(std::move(_effects))
        {}

    SourceEffectGroup(const SourceEffectGroup&) = delete;
    SourceEffectGroup& operator = (const SourceEffectGroup&) = delete;

    EffectStatus virtual active(const World& world);
    void virtual activate(World& world);

    friend Character;
};

class TargetEffectGroup{
    Character * const source;
    Character * target;

    public:
    std::vector<std::unique_ptr<EffectST>> effects;
    private:

    const AbilityTargetting target_types;

    public:
    TargetEffectGroup(Character* _source, Character* _target, std::vector<std::unique_ptr<EffectST>> _effects, AbilityTargetting _target_types):
        source(_source),
        target(_target),
        effects(std::move(_effects)),
        target_types(_target_types)
        {}

    bool virtual active(
        PlayerCharacter& per,
        // const std::vector<AICharacter<RandomAI>*>& friendlies,
        const std::vector<AICharacter<RandomAI>*>& hostiles,
        const World& world
        );
    void virtual activate(World& world);
    bool validTarget(const Character& tc, const World& world) const;
    void reset();

    void calculateTargets(
        std::vector<Character*>& targets,
        PlayerCharacter& per,
        // const std::vector<AICharacter<RandomAI>*>& friendlies,
        const std::vector<AICharacter<RandomAI>*>& hostiles,
        const World& world
        );

    friend Ability;
};

#endif // EFFECTGROUP_HPP_INCLUDED
