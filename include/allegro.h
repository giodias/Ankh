#ifndef ALLEGRO_H
#define ALLEGRO_H
#include <allegro5/allegro.h> //the core functions
//#include <allegro5/allegro_main.h> //a compatibility libarary that allows int main() to work across all compilers. Do not confuse this with the base allegro library. It does not include any functionality. It is only required on OS X.
#include <allegro5/allegro_audio.h> //the basic audio subsystem
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_acodec.h> //audio codecs (e.g., .OGG, .WAV)
//#include <allegro5/allegro_color.h> //converting between various color formats
//#include <allegro5/allegro_dialog.h> //native dialogs (e.g., Open File)
#include <allegro5/allegro_font.h> //basic bitmap fonts
#include <allegro5/allegro_image.h> //image formats (e.g., .JPEG, .PNG)
//#include <allegro5/allegro_memfile.h> //an interface to load files from memory
//#include <allegro5/allegro_physfs.h> //an interface to load files from abstract locations (e.g., a .ZIP file)
//#include <allegro5/allegro_primitives.h> //basic drawing functions (e.g., rectangles, lines)
#include <allegro5/allegro_ttf.h> //loading .TTF fonts
#endif // ALLEGRO_H
