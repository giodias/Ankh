#ifndef BUFF_HPP_INCLUDED
#define BUFF_HPP_INCLUDED

#include <sqlite3.h>
#include "allegro.h"

#include "EffectGroup.hpp"
#include "Ability.hpp"
#include "Effect.hpp"
#include "EADisplay.hpp"

class Buff: public EADisplay {

    public:
    Buff(Character* _source, string _name, bool _show, std::vector<unique_ptr<Effect>> _effects, bool _use_color=false, ALLEGRO_COLOR _color=al_map_rgba(255,255,255,255), ALLEGRO_BITMAP* _bitmap=nullptr):
        EADisplay(_name, _show),
        effects(_source, std::move(_effects)),
        use_color(_use_color),
        color(_color),
        bitmap(_bitmap)
        {}
    virtual ~Buff(){
        al_destroy_bitmap(bitmap);
    }

    SourceEffectGroup effects;

    bool use_color;
    ALLEGRO_COLOR color;
    ALLEGRO_BITMAP* bitmap;

    bool virtual end_turn(int turn, World& world); //false is buff ended

    void storegame(sqlite3* db, std::string& savename);
    void loadgame(sqlite3* db, std::string& savename);
};

class TemporaryBuff: public Buff {
    protected:
    int turns_remaining;

    public:
    TemporaryBuff(Character* _source, string _name, bool _show, int _turns_remaining, std::vector<unique_ptr<Effect>> _effects, bool _use_color=false, ALLEGRO_COLOR _color=al_map_rgba(255,255,255,255), ALLEGRO_BITMAP* _bitmap=nullptr):
        Buff(_source, _name, _show, std::move(_effects), _use_color, _color, _bitmap),
        turns_remaining(_turns_remaining)
        {}
    virtual ~TemporaryBuff(){
        al_destroy_bitmap(bitmap);
    }

    bool virtual end_turn(int turn, World& world);
};

#endif // BUFF_HPP_INCLUDED
