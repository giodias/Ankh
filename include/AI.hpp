#ifndef AI_HPP_INCLUDED
#define AI_HPP_INCLUDED

#include <random>

#include "../include/allegro.h"
#include "Character.hpp"

class PlayerCharacter;
template <typename T> class AICharacter;

class RandomAI {
    AICharacter<RandomAI> * aichar;

    public:
    RandomAI(AICharacter<RandomAI>* _aichar):
        aichar(_aichar)
        {}

    void EnemyIA(PlayerCharacter* per, std::vector<AICharacter<RandomAI>*> hostiles, World& world);
};

#endif //AI_HPP_INCLUDED
