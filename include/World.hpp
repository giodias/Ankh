#ifndef WORLD_HPP
#define WORLD_HPP

#define TILE_SIZE 32

#include <vector>
#include <map>
#include <fstream>
#include "../include/allegro.h"
#include "../include/Entity.hpp"
using namespace std;
class World {
    int width, height, tileSize, nextId;
    coord screenPos;
    ALLEGRO_BITMAP *background;
    vector<Entity*> visibleEntities;
    map<int, Entity*> entities;
public:
    World(char const *name, int width, int height, coord screen = Coord{0, 0}, int tileSize = TILE_SIZE);
    ~World();
    map<int, Entity*> *getEntities();
    int *getNumEntities();
    void setScreenPos(coord c);
    coord getScreenPos();
    void insertEntity(Entity *ent);
    static bool sortFunction(Entity *i, Entity *j);
    void resetEntities();
    void selectVisibleEntities(coord origin, int width, int height);
    void drawEntities(coord origin);
    void handleColision();
    bool inBorder(Entity &ent, Direction dir);
    bool inCell(Entity &ent, coord pos, Direction dir);
    int getWidth();
    int getHeight();
    int getRealWidth();
    int getRealHeight();
    Coord random_position();
    int getTileSize() const;
    void draw(coord origin, int viewWidth, int viewHeight);
    void ResetID(void){this->nextId = 0;}
};
#endif // WORLD_HPP
