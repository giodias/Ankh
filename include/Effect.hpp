#ifndef EFFECT_HPP_INCLUDED
#define EFFECT_HPP_INCLUDED

#include <random>
#include <functional>

#include "Buff.hpp"
#include "Ability.hpp"
#include "Character.hpp"
#include "World.hpp"

class Character;
class Buff;

enum class EffectStatus {impossible, pointless, useful};

EffectStatus joinEffectStatus(EffectStatus a, EffectStatus b);

class Effect {
    public:
    virtual EffectStatus valid_target(const Character& target, const World& world) = 0;
    virtual void activate(Character& source, World& world) = 0;

    virtual ~Effect(){}
};

class EffectST {
    public:
    virtual EffectStatus valid_target(const Character& source, const Character& target, const World& world) = 0;
    virtual void activate(Character& source, Character& target, World& world) = 0;

    virtual ~EffectST(){}
};

class EffectS: public EffectST {
    unique_ptr<Effect> effect;
    public:
    virtual EffectStatus valid_target(const Character& source, const Character& target, const World& world);
    virtual void activate(Character& source, Character& target, World& world);

    EffectS(unique_ptr<Effect> _effect):
        effect(std::move(_effect))
        {}

    virtual ~EffectS(){}
};

class EffectT: public EffectST {
    unique_ptr<Effect> effect;
    public:
    virtual EffectStatus valid_target(const Character& source, const Character& target, const World& world);
    virtual void activate(Character& source, Character& target, World& world);

    EffectT(unique_ptr<Effect> _effect):
        effect(std::move(_effect))
        {}

    virtual ~EffectT(){}
};

class RotateToTarget: public EffectST  {
    virtual EffectStatus valid_target(const Character& source, const Character& target, const World& world);
    virtual void activate(Character& source, Character& target, World& world);
};

class EndTurn: public Effect {
    virtual EffectStatus valid_target(const Character& target, const World& world);
    virtual void activate(Character& source, World& world);

    virtual ~EndTurn(){}
};

class AlwaysPointless: public virtual Effect {
public:
    virtual EffectStatus valid_target(const Character& target, const World& world);
    virtual ~AlwaysPointless(){}
};

class AlwaysValidTarget: public virtual Effect {
    public:
    virtual EffectStatus valid_target(const Character& target, const World& world);
    virtual ~AlwaysValidTarget(){}
};

class StatsEffect : public virtual Effect {
    protected:
    int const hp_effect;
    int const mp_effect;
    int const ap_effect;

    public:
    StatsEffect(int _hp_effect, int _mp_effect, int _ap_effect):
        hp_effect(_hp_effect), mp_effect(_mp_effect), ap_effect(_ap_effect)
        {}
    virtual ~StatsEffect(){}

    virtual void activate(Character& source, World& world);
};

class FriendlyStatsEffect final: public StatsEffect {
    virtual EffectStatus valid_target(const Character& target, const World& world);

    public:
    FriendlyStatsEffect(int _hp_effect, int _mp_effect, int _ap_effect):
        StatsEffect(_hp_effect, _mp_effect, _ap_effect)
        {}
    virtual ~FriendlyStatsEffect(){}
};

class HostileStatsEffect final: public virtual StatsEffect, public virtual AlwaysValidTarget {
    public:
    HostileStatsEffect(int _hp_effect, int _mp_effect, int _ap_effect):
        StatsEffect(_hp_effect, _mp_effect, _ap_effect)
        {}
    virtual ~HostileStatsEffect(){}
};

class RandomTeleport final: public AlwaysValidTarget {
    public:
    void activate(Character& source, World& world);

    virtual ~RandomTeleport(){}
};

class Animate: public AlwaysPointless {
    public:
    EntityStateAction animation_action;
    virtual void activate(Character& source, World& world);

    Animate(EntityStateAction _animation_action):
        animation_action(_animation_action)
    {}
    virtual ~Animate(){}
};

class AnimateIfDead final: public Animate {
    public:
    void activate(Character& source, World& world);

    AnimateIfDead(EntityStateAction _animation_action):
        Animate(_animation_action)
    {}
    virtual ~AnimateIfDead(){}
};

class Move final: public Effect {
    Direction const direction;

    public:
    Move(Direction _direction):
        direction(_direction)
        {}
    virtual ~Move(){}

    void activate(Character& source, World& world);
    EffectStatus valid_target(const Character& target, const World& world);
};

class BuffEffect final: public AlwaysPointless {
    std::function<unique_ptr<Buff>(Character&)> buffgen;

    public:
    BuffEffect(std::function<unique_ptr<Buff>(Character&)>  _buffgen):
        buffgen(_buffgen)
        {}
    virtual ~BuffEffect(){}

    void activate(Character& source, World& world);
};

#endif //EFFECT_HPP_INCLUDED
