#ifndef STAT_CPP_INCLUDED
#define STAT_CPP_INCLUDED

#include <string>

#include "../include/allegro.h"

template <typename T> class Number {

    protected:
    T value;

    public:
    operator T();

    bool operator ==(const T o) const;
    bool operator !=(const T o) const;
    bool operator <=(const T o) const;
    bool operator <(const T o) const;
    bool operator >(const T o) const;
    bool operator >=(const T o) const;

    void operator +=(const T o);
    void operator -=(const T o);
    void operator =(const T o);

    T operator +(const T o) const;

    std::string toString() const;

    Number<T>(T t):
    value(t)
        {}
};

template <typename T> struct Stat: public Number<T> {
    Number<T> maxValue;

    std::string toString() const;

    Stat(int _value, int _maxValue):
        Number<T>(_value),
        maxValue(_maxValue)
        {}
    Stat(int _maxValue):
        Number<T>(_maxValue),
        maxValue(_maxValue)
        {}

    void operator +=(const T o);
    void operator -=(const T o);
    void operator =(const T o);

    void setToMax();
    bool atMax() const;

    void bDiff(const T o);

    void draw(ALLEGRO_FONT* ability_font, int posX, int posY, std::string prefix) const;
};


#endif //STAT_CPP_INCLUDED
