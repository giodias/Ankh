#ifndef SCREEN_HPP
#define SCREEN_HPP
#include "../include/Entity.hpp"
#include "../include/World.hpp"
class Screen {
    coord position;
    int width, height;
    World *world;
public:
    Screen(World &world, int width, int height);
    ~Screen();
    void setWorld(World &world);
    World *getWorld();
    void clearBuffer();
    void updatePosition(const Entity &character);
    void selectVisible();
    void draw();
};
#endif // SCREEN_HPP
