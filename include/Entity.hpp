#ifndef ENTITY_HPP
#define ENTITY_HPP
#include <algorithm>
#include <map>
#include "../include/config.hpp"
#include "../include/allegro.h"

#define ANIMATION_SPEED 0.4
#define DEFAULT_COLOR al_map_rgba(255, 255, 255, 255)

using namespace std;

extern int frameCounter;

enum WhichClass {_Entity, _Character};//entities here

enum class EntityStateDirection {
    up=0, left, down, right
};
enum class EntityStateAction {
  casting=0, thrust, walk, slash, shoot, hurt
};
int NumberOfPhases(EntityStateAction esa);
typedef int EntityStatePhase;

enum Direction {_stop, _up, _left, _down, _right};
EntityStateDirection dir2esa(Direction d);

typedef struct Coord {
    int x, y;
    Coord() {};
    Coord(int _x, int _y) : x(_x), y(_y) {};
    bool operator ==(const Coord &c) {
        return this->x == c.x && this->y == c.y;
    }
    bool operator !=(const Coord &c) {
        return !(*this == c);
    }
    int distance(const Coord &c) {
        return max(abs(this->x - c.x), abs(this->y - c.y));
    }
} coord;

class Entity {
protected:
    static map<int, Entity*> *entities;
    static int worldWidth, worldHeight;
    static int *nextId;
    static int tileSize;
    int id;
    WhichClass name;
    int width, height;
    coord position, initialPosition;
    ALLEGRO_BITMAP *sprite;
    bool blocked;
    int velocity;
public:

    static int screenWidth;
    static int screenHeight;

    EntityStateDirection entitystatedirection;
    EntityStateAction entitystateaction;
    EntityStatePhase entitystatephase;

    Entity(char const * path, int width, int height, int x = 0, int y = 0,
           EntityStateDirection esd = EntityStateDirection::down,
           EntityStateAction esa = EntityStateAction::walk,
           EntityStatePhase esp = 0
           );
    virtual ~Entity();
    void insertWorld();
    void setId(int id);
    coord getInitialPos();
    int getX() const;
    int getRealX(int x, int y);
    int getY() const;
    int getRealY(int y);
    coord getPosition();
    int getWidth();
    int getHeight();
    float getScale(int y);
    void setPosition(coord p);
    virtual void reset();
    void block();
    void unblock();
    int distance(const Entity &e) const;
    int distanceX(const Entity &e) const;
    int distanceY(const Entity &e) const;
    virtual bool isAlive() const;
    bool canMove(int dx, int dy) const;
    bool updatePosition(int dx, int dy);
    virtual void drawSprite(coord origin, ALLEGRO_COLOR color=DEFAULT_COLOR);
    virtual void drawOtherSprite(ALLEGRO_BITMAP* bitmap, coord origin, ALLEGRO_COLOR color=DEFAULT_COLOR);
    void drawOnTop(char const * const text, ALLEGRO_FONT* font, coord origin, ALLEGRO_COLOR color);
    WhichClass getClass() const;
    bool collide(Entity &ent, coord p) const;
    Direction backDirection(Entity &ent);
    Direction closestDirection(const Entity &ent);
    static void setNewMap(map<int, Entity*> *ent, int w, int h, int *next, int tileSize);
    void phaseUpdate(bool end);
    void move(Direction dir);
    virtual void end_turn(int){};
    static int getTileSize();
};
#endif // ENTITY_HPP
