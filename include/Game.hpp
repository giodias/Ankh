#ifndef GAME_HPP_INCLUDED
#define GAME_HPP_INCLUDED

#include <vector>
#include <memory>

#include <sqlite3.h>

#include "../include/World.hpp"
#include "../include/Screen.hpp"
#include "../include/Character.hpp"
#include "../include/Character.hpp"

enum State { playing, targeting, gameover,win}; // TODO? enum class

struct Game {
    int level;
    int turn;
    int killcount;

    World mundo;
    std::vector<AICharacter<RandomAI>*> hostiles;
    Screen tela;

    State state = playing;
    PlayerCharacter* per;

    unique_ptr<Ability>* selected_ability;

    std::uniform_real_distribution<float> rand_create;
    float generate_enemies_chance;

    std::vector<Character*> targets;

    bool loop = true;
    bool isPaused = true;
    bool isPlayerTurn = true;
    bool redraw = true;

  bool& isGUIopen;
  GUI*& openGUI;

    ALLEGRO_TIMER* screen_update_timer; //receber?
    ALLEGRO_TIMER* tu_timer; //receber?

    ALLEGRO_FONT* ability_font;// receber
    ALLEGRO_FONT* target_font;// receber
    ALLEGRO_FONT* big_font;// receber

    ALLEGRO_EVENT_QUEUE* event_queue; // receber

    ALLEGRO_AUDIO_STREAM* audio_stream;

    ALLEGRO_DISPLAY* display = NULL;

    LIB_GUI* guiLibrary; // receber


    void mainloop();
    void restart(bool fullreset = true);
    void start(bool fullreset = true);
    void startGame();
    void endGame();

  void loadgame(sqlite3* db, int id);
  void storegame(sqlite3* db, int slot, std::string& name);

    ~Game();
    Game(
        bool& _isGUIopen,
        GUI*& _OpenGUI,
        ALLEGRO_TIMER* _screen_update_timer, //receber?
        ALLEGRO_TIMER* _tu_timer, //receber?

        ALLEGRO_FONT* _ability_font,// receber
        ALLEGRO_FONT* _target_font,// receber
        ALLEGRO_FONT* _big_font,// receber

        ALLEGRO_EVENT_QUEUE* _event_queue, // receber

        ALLEGRO_AUDIO_STREAM* _audio_stream,

        ALLEGRO_DISPLAY* _display,

        LIB_GUI* _guiLibrary // receber
    );
};

#endif // GAME_HPP_INCLUDED
