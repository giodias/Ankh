/**============================================================================
 *  Project Name    <Ankh>
 *  Version         <1.0>
 *  Authors         <all members>
 *  File Name       <GUI.hpp>
/=============================================================================*/
#ifndef GUI_HPP_INCLUDED
#define GUI_HPP_INCLUDED
/**==========================================================================**/
#include <vector>
#include <memory>
#include <functional>
#include <allegro5/allegro5.h>
#include <stdarg.h>
#include <iostream>
#include "../include/util.hpp"
#include "../include/config.hpp"
/**==========================================================================**/
using namespace std;

namespace BMPL{
    enum {MAIN_MENU,BUTTON,MENU,CURSOR,SLIDER,CREDITOS};
}
namespace GUIL{
    enum{MAIN_MENU=0,PAUSE_MENU,CONFIG_MENU,CREDITOS,HIGHSCORES,LOAD,SAVELOAD,MMCONFIG,ENDGAME};
}
/**==========================================================================**/
class GUI;
class GUIELEMENT;
class BUTTON;
class SLIDEBAR;

void changeToGui(GUI*& opGUI, GUI* toGUI);
/**==========================================================================**/
class GUIELEMENT{
    public:
        const bool isClickable,isMouseUpdate;
        bool isPress, isSelected;
        int pX,pY,w,h;
        GUIELEMENT(int _px, int _py,int _w,int _h,bool click,bool mupd):
                   isClickable(click),isMouseUpdate(mupd),isPress(false),
                   isSelected(false),pX(_px),pY(_py),w(_w),h(_h){}
        virtual ~GUIELEMENT(){}
        virtual void draw(int& ox,int& oy) = 0;
        virtual void ClickIn(void)=0;
        virtual void move(int& mX,int& mY,int& oX,int &oY)=0;
        virtual void reset() = 0;
        virtual void UnicharReceive(int uch) = 0;
        friend GUI;
};
/**==========================================================================**/
class BUTTON : public GUIELEMENT{
    private:
        ALLEGRO_BITMAP* background;
        ALLEGRO_FONT* font;
        ALLEGRO_COLOR color;
        std::function<void()> inFunction;

    public:
        string text;
        BUTTON(int _px, int _py,int _w,int _h,ALLEGRO_BITMAP* _background,ALLEGRO_FONT* _font,
               ALLEGRO_COLOR _color,string _text, std::function<void()> _inFunction);
        ~BUTTON();
        void draw(int& ox,int& oy);
        void ClickIn(void){if(inFunction) inFunction();}
        void move(int&,int&,int&,int&){} //void move(int& mX,int& mY,int& oX,int &oY){}
        void reset();
        void UnicharReceive(int uch);
        friend GUI;
};
/**==========================================================================**/
class SLIDEBAR : public GUIELEMENT{
    private:
        ALLEGRO_BITMAP* cursor;
        ALLEGRO_BITMAP* background;
        ALLEGRO_FONT* font;
        const bool isHorizontal;
        double& value;
        int wc;
        ALLEGRO_COLOR color;
        std::string text;
    public:
        SLIDEBAR(int _px, int _py,int _w,int _h,int _wc,ALLEGRO_BITMAP* _cursor,ALLEGRO_BITMAP* _background,
                bool _isHorizontal,double& value,ALLEGRO_FONT* _font,
               ALLEGRO_COLOR _color, std::string _text);
        ~SLIDEBAR(){}
        void draw(int& ox,int& oy);
        void ClickIn(void){}
        void move(int& mX,int& mY,int& oX,int& oY);
        void reset();
        void UnicharReceive(int uch);
        friend GUI;
};
/**==========================================================================**/
class TEXT : public GUIELEMENT{
protected:
  ALLEGRO_COLOR textcolor;
  ALLEGRO_FONT* font;
public:
  std::string text;
  TEXT(int _px, int _py,int _w,int _h,ALLEGRO_FONT* _font,
          ALLEGRO_COLOR _textcolor,std::string _inittext);
  ~TEXT();
  void draw(int& ox,int& oy);
  void ClickIn(){}
  void move(int&,int&,int&,int&){} //void move(int& mX,int& mY,int& oX,int &oY){}
  void reset(){}
  void UnicharReceive(int ){}
  friend GUI;
};
/**==========================================================================**/
class TEXTBOX : public TEXT{
private:
        ALLEGRO_COLOR backcolor;
public:
        std::string deftext;
        TEXTBOX(int _px, int _py,int _w,int _h,ALLEGRO_FONT* _font,
               ALLEGRO_COLOR _backcolor,ALLEGRO_COLOR _textcolor,std::string _inittext);
        ~TEXTBOX();
        void draw(int& ox,int& oy);
        virtual void ClickIn(void);
        void move(int&,int&,int&,int&){} //void move(int& mX,int& mY,int& oX,int &oY){}
        void reset();
        void UnicharReceive(int uch);
        friend GUI;
};

/**==========================================================================**/
template <size_t COL>
class TABLE : public GUIELEMENT{
    private:
        ALLEGRO_FONT* font;
        ALLEGRO_COLOR backcolor;
        ALLEGRO_COLOR textcolor;
  int line_height;
        std::function<std::vector<std::array<std::unique_ptr<GUIELEMENT>,COL>>()> genline;
    public:
        TABLE(int _px, int _py,int _w,int _h,ALLEGRO_FONT* _font,
              ALLEGRO_COLOR _backcolor,ALLEGRO_COLOR _textcolor,
              std::array<std::pair<std::string,int>,COL> cols,
              std::function<std::vector<std::array<std::unique_ptr<GUIELEMENT>,COL>>()> genline,
              int line_height
              );
        ~TABLE() = default;
        std::vector<std::array<std::unique_ptr<GUIELEMENT>,COL>> elements;
        std::array<std::pair<std::string,int>,COL> cols;
        void draw(int& ox,int& oy);
        virtual void ClickIn(void);
        void move(int&,int&,int&,int&){} //void move(int& mX,int& mY,int& oX,int &oY){}
        void reset();
        void UnicharReceive(int uch);
        friend GUI;
};
/**==========================================================================**/

class GUI{
    private:
        ALLEGRO_BITMAP* background;
        vector<unique_ptr<GUIELEMENT>> elements;
        int w,h;
        std::function<void()> extraDraw;
    public:
        const bool isClickableGUI,isMouseUpdateGUI;
        const string id;
        GUI(string _id,int _w,int _h,ALLEGRO_BITMAP* _background,
            bool _isClickable,bool _isMouseUpdateGui, std::function<void()> _extraDraw = nullptr);
        void draw(void);
        void move(int mX,int mY);
        void ClickIn(int mX, int mY);
        void ClickOut(void);
        void add(GUIELEMENT* eg);
        void reset();
        void ResetSelection(void);
        void UnicharReceive(int uch);

};
/**==========================================================================**/

template class TABLE<4>;
template class TABLE<3>;
template class TABLE<2>;

#endif
